<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<!-- Acts as interface for the calulator app and displays past calculations.-->
<html>
  <head>
    <title>A1</title>
    <style>

      
      body
      {
          background-color: grey;
          margin: auto;
          padding-top: 20px;
          padding-bottom: 20px;
          text-align: center;
          background-image:url(https://cosc349-a2-db.s3.amazonaws.com/imgs/stockImage3.jpg);
}
      table
      {
          margin: auto;
          padding: auto;
          text-align: center;
      }
      tr:last-child
      {
          background:#66ccff;
      }
      #wrapper
      {
          background-color: white;
          padding-top: 0px;
          width: 1000px;
          height: 100%;
          margin: 0px auto;
      }
      #calculatorborder
      {
          
          background-color: lightgrey;
          margin: 25px auto;
          width: 500px;
          border-style: solid;
          padding: 10px;
          
      }
      
      #header
      {
          background-color: green;
          padding-top: 10px;
          margin: 0px;
          width: 1000px;
          height: 80px;
          top: 0px;
          color: #FFC733;
          border-bottom: 4px solid black;
          
      }
      #tableBorder {
          background-color: blue;
          width: 320px;
          margin: 0px auto;
          overflow: auto;
          height: 500px;
          padding: 10px;
          background-color: white;
      }
      
    </style>
  </head>

  <body>
    <div id="wrapper">
      <div id="header">
        <h1> COSC349 CALCULATOR INTERFACE WEBSERVER </h1>
      </div>
      
      <div id="calculatorborder">
        <h2>Calculator </h2>
        <form method="post" action="calHere.php">
          Enter First Number:
          <input type="number" name="number1" /><br><br>
          Enter Second Number:
          <input type="number" name="number2" /><br><br>
          <input  type="submit" name="submit" value="Add">
        </form>
    
      </div>
      <img src="https://cosc349-a2-db.s3.amazonaws.com/imgs/stockImage1.jpeg" alt="Maths stock image">
	  <img src="https://cosc349-a2-db.s3.amazonaws.com/imgs/stockImage2.jpg" alt="Maths stock image 2" width="300" height="172">      
        <h3>Addition history table. The last input is in the light blue row</h3> 

      <div id="tableBorder">
        <table border="1">
<tr><th>First Number</th><th>Second Number</th><th>Sum</th></tr>

<?php
 
$RDS_host   = 'database349.c78dmyxdh8vi.us-east-1.rds.amazonaws.com';
    $RDS_name   = 'database349';
    $RDS_port   = 3306;    
    $RDS_user   = 'admin';
    $RDS_passwd = 'adminadmin';
    $charset = 'utf8' ;

    $pdo_dsn = "mysql:host=$RDS_host;port=$RDS_port;dbname=$RDS_name;charset=$charset";

    $pdo = new PDO($pdo_dsn, $RDS_user, $RDS_passwd);

$q = $pdo->query("SELECT * FROM adding");
while($row = $q->fetch()){
  echo "<tr><td>".$row["FNum"]."</td><td>".$row["SNum"]."</td><td>".$row["Sum"]."</td></tr>";
}

   
?>
</table>
      </div>
<form method="post" action="cleartable.php">
          <input type="hidden" name="nothing" value="nothing" />
            <input  type="submit" name="submit" value="clear table">
          <br>
          
        </form>
    </div>

  </body>
</html>
